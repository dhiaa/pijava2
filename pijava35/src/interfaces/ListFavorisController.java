/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import com.itextpdf.text.Document;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.pdf.PdfWriter;
import entities.Favoris_client;
import entities.article;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.URL;
import static java.nio.file.Files.list;
import static java.util.Collections.list;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import services.ServiceArticle;
import services.ServiceFavoris;

/**
 * FXML Controller class
 *
 * @author user
 */
public class ListFavorisController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private ListView<Favoris_client> listefav;
    ObservableList<Favoris_client> data;
    @FXML
    private Button ret;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        data = FXCollections.observableArrayList();
        loadDataFromDatabase();
        listefav.setCellFactory(lv -> new Poules());

    }

    @FXML
    private void retourart(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/interfaces/produits.fxml"));
            Scene scene = new Scene(root);
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.setScene(scene);
            app_stage.show();
        } catch (IOException ex) {
        }
    }

    @FXML
    private void generer(ActionEvent event) {
        try {
            ServiceFavoris sf = new ServiceFavoris();
            List pdf = sf.Afficher_favoris();
            
            int x=0 ;
    //// GENERATE PDF
    String k = 
            "<head><img src=\"C:\\Users\\user\\Documents\\GitHub\\pijava\\pijava3\\src\\ressources\\image\\cite-culture.jpg\" alt=\"Smiley face\" height=\"100\" width=\"100\"><head/>"
            +"<html><body><br/> <h1>ci-joint la liste de vos favoris </h1><br/>"
            + " <br> <h4 > nom article           realisateur :  </h1> <br/> "+pdf.toString()
            + "      "
            + "        </body></html>";
    File currDir = new File(System.getProperty("user.dir", "."));
    String path = currDir.toPath().getRoot().toString() + "wamp64/www/pidevsymfony1/web/upload/test2.pdf";
    OutputStream file = new FileOutputStream(new File(path));
    
    Document document = new Document();
    PdfWriter.getInstance(document, file);
    document.open();
    HTMLWorker htmlWorker = new HTMLWorker(document);
    htmlWorker.parse(new StringReader(k));
    document.close();
    file.close();
    /// END GENERATE PDF
    
    /// OPEN PDF
    File f = new File(path);
    Desktop.getDesktop().open(f);
    //END OPEN PDF
} catch (Exception e) {
    e.printStackTrace();
} 
        
        
        
    }

    

    public class Poules extends ListCell<Favoris_client> {

        public Poules() {

        }

        @Override
        protected void updateItem(Favoris_client item, boolean bln) {
            super.updateItem(item, bln);

            if (item != null) {

                Text t5 = new Text(item.getDesc());
                Text t3 = new Text(item.getReal());
                Button btn = new Button("supprimer");
                //System.out.println("ssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
                // btn.setOnAction(value);
                btn.setOnAction(new javafx.event.EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        ServiceFavoris sf = new ServiceFavoris();

                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                        alert.setTitle("Suppression article ");
                        alert.setHeaderText("");
                        alert.setContentText("Voulez vous supprimer cet article ?");

                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.get() == ButtonType.OK) {
                            sf.Supprimer_favoris(item.getId_fav());
                            try {
                                Parent root = FXMLLoader.load(getClass().getResource("/interfaces/produits.fxml"));
                                Scene scene = new Scene(root);
                                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                app_stage.setScene(scene);
                                app_stage.show();
                            } catch (IOException ex) {

                            }

                        } else {
                            updateItem(item, true);

                        }

                    }
                    
                });

                t3.setStyle("-fx-font-size: 20 arial;");
                t5.setStyle("-fx-font-size: 20 arial;");
                VBox vBox = new VBox(t5, t3);
                vBox.setSpacing(40);
                 ServiceFavoris ssf = new ServiceFavoris();
                 String  imgg = ssf.imagefav(item.getId_art());
                 System.out.println("**********");
                 File file = new File("C:\\wamp64\\www\\pidevsymfony1\\web\\uploads\\images", imgg);
                
                Image image = new Image(file.toURI().toString());
                ImageView img = new ImageView(image);
                img.setFitHeight(100);
                img.setFitWidth(80);
                //File file = new File("C:\\wamp64\\www\\pidevsymfony1\\web\\uploads\\images", item.getImage());
                //Image image = new Image(file.toURI().toString());
                //ImageView img = new ImageView(image);
                // img.setFitHeight(200);
                // img.setFitWidth(160);
                
                HBox h1Box = new HBox(vBox,btn);
                h1Box.setSpacing(500);
                HBox hBox = new HBox(img,h1Box);

                hBox.setSpacing(10);
                setGraphic(hBox);
            }
        }
    }

    private void loadDataFromDatabase() {
        try {
            ServiceFavoris service = new ServiceFavoris();

            List<Favoris_client> rs = service.Afficher_favoris();
            System.out.println(rs);
            for (Favoris_client a : rs) {
                Favoris_client r = new Favoris_client();
                r.setId_fav(a.getId_fav());
                r.setId_art(a.getId_art());
                r.setId_user(a.getId_user());
                r.setDesc(a.getDesc());
                r.setReal(a.getReal());

                data.add(r);
                System.out.println("recup table favoris view ok !");

            }

        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
        listefav.setItems(data);
        System.out.println(data + "load data ****");

    }

}
