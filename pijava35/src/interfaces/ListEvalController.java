/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import entities.Evaluation;
import entities.article;
import entities.user;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.KeyEvent;
import services.ServiceArticle;
import services.ServiceEvaluation;
import utils.MyConnection;

/**
 * FXML Controller class
 *
 * @author user
 */
public class ListEvalController implements Initializable {
    Connection C = MyConnection.getInstance().getConn();
        private static ResultSet r;

    private TextField recherche;
    @FXML
    private TableColumn<Evaluation, String> nom_user;
    @FXML
    private TableColumn<Evaluation, String> nom_art;
    @FXML
    private TableColumn<Evaluation, Double > not;
    @FXML
    private Button statt;
    @FXML
    private TableView<Evaluation> tab;
    @FXML
    private PieChart picharteval;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        aff();

        ServiceEvaluation sa = new ServiceEvaluation();
        
      
    
        
    }    
            

      private void aff() {
    
    nom_user.setCellValueFactory(new PropertyValueFactory<>("username"));
    nom_art.setCellValueFactory(new PropertyValueFactory<>("nomProduit"));
    not.setCellValueFactory(new PropertyValueFactory<>("note"));
    
        //oblist=sa.getEvalByIduser("ggg")
        ServiceEvaluation sa = new ServiceEvaluation(); //tab_art.setItems(new ImageView(image1));
        // tab_art.setEditable(true);
        //desc_col.setCellFactory(TextFieldTableCell.forTableColumn());
        ObservableList<Evaluation> oblist = FXCollections.observableArrayList();
        oblist=sa.findAll();
        tab.setItems(oblist);
    
    
    }
    
    
    

    @FXML
    private void showstat(ActionEvent event) {
        
        ObservableList<Evaluation> oblist = FXCollections.observableArrayList();
        ServiceEvaluation se=new ServiceEvaluation();
       oblist = se.findAll();
       for (Evaluation e : oblist) {  
            System.out.println(e);
        }
      
        ServiceEvaluation sa = new ServiceEvaluation();
      
       sa.top5nomart();
       sa.top5moy();
        ObservableList<PieChart.Data> pieChartData =
                FXCollections.observableArrayList(
                new PieChart.Data(sa.top5nomart().get(0).getDescription(), sa.top5moy().get(0)),
                new PieChart.Data(sa.top5nomart().get(1).getDescription(), sa.top5moy().get(1)),
                new PieChart.Data(sa.top5nomart().get(2).getDescription(), sa.top5moy().get(2)),
                new PieChart.Data(sa.top5nomart().get(3).getDescription(), sa.top5moy().get(3)),
                new PieChart.Data(sa.top5nomart().get(4).getDescription(), sa.top5moy().get(4)));
                
                
        picharteval.setData(pieChartData);
        
        }
   /* ObservableList<Evaluation> dataa = FXCollections.observableArrayList();
    ServiceEvaluation seval = new ServiceEvaluation();
    
    // ObservableList<Evaluation> oblist = FXCollections.observableArrayList();
    String requete = "select * from evaluation ";
    try {
    Statement statement = C.prepareStatement(requete);
    r = statement.executeQuery(requete);
    while (r.next()) {
    Evaluation f = new Evaluation();
    f.setIduser(r.getInt("iduser"));
    f.setIdprod(r.getInt("idprod"));
    f.setIdevaluation(r.getInt("idevaluation"));
    f.setNote(r.getDouble("note"));
    
    //article a = this.getarticle(r.getInt(2));
    //user uu = this.getuser(r.getInt(4));
    // f.setNomProduit(a.getDescription());
    //f.setUsername(uu.getUsername());
    System.out.println(f.toString());
    dataa.add(f);
    }
    
    } catch (SQLException ex) {
    System.out.println("SQL Error: " + ex);
    
    }
    
    
    
    // dataa =seval.findAll();
    
    
    PieChart pee = new PieChart();
    pee.setData();
    
    seval.moyByName(0);
    */
    }
    
    
    
    

    

