/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import entities.article;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import services.ImageService;
import services.ServiceArticle;
import utils.MyConnection;

/**
 * FXML Controller class
 *
 * @author user
 */
public class AffAllArtController implements Initializable {

    Connection C = MyConnection.getInstance().getConn();
    private String imageee;

    @FXML
    private TableView<article> tab_art;
    @FXML
    private TableColumn<article, Integer> id_col;
    @FXML
    private TableColumn<article, String> desc_col;
    @FXML
    private TableColumn<article, Double> prix_col;
    @FXML
    private TableColumn<article, Date> date_col;
    @FXML
    private TableColumn<article, String> img_col;
    @FXML
    private TableColumn<article, String> real_col;
    @FXML
    private TextField prix;
    @FXML
    private TextField desc;
    @FXML
    private Button boutonModifier;
    @FXML
    private TextField real;
    @FXML
    private TextField img;
    @FXML
    private AnchorPane modif;
    @FXML
    private TextField recherche;
    @FXML
    private TextField desc1;
    @FXML
    private TextField prix1;
    @FXML
    private DatePicker dateField;
    private TextField img1;
    @FXML
    private TextField real1;
    @FXML
    private Button boutonModifier1;
    @FXML
    private AnchorPane ajout;
    @FXML
    private DatePicker date_col5;
    @FXML
    private Button img5;
    @FXML
    private TextField chemin;
    @FXML
    private Button consulter;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Aff();
        ajout.setVisible(true);

    }

    private void Aff() {

        ajout.setVisible(true);
        modif.setVisible(false);
        id_col.setCellValueFactory(new PropertyValueFactory<>("id"));
              desc_col.setCellValueFactory(new PropertyValueFactory<>("description"));
              prix_col.setCellValueFactory(new PropertyValueFactory<>("prix"));
              date_col.setCellValueFactory(new PropertyValueFactory<>("date"));
              img_col.setCellValueFactory(new PropertyValueFactory<>("img"));
              real_col.setCellValueFactory(new PropertyValueFactory<>("realisateur"));
        try {
            ServiceArticle sa = new ServiceArticle();
            ObservableList<article> oblist = FXCollections.observableArrayList();
            oblist=sa.afficherImage();
            tab_art.setItems(oblist);

        } catch (SQLException ex) {
            System.out.println("erreur" + ex);
        }
        
        //tab_art.setItems(new ImageView(image1));

        tab_art.setEditable(true);
        desc_col.setCellFactory(TextFieldTableCell.forTableColumn());

    }

    @FXML
    private void supprimerArt(MouseEvent event) {
        article S = (article) tab_art.getSelectionModel().getSelectedItem();
        ServiceArticle serv = new ServiceArticle();
        String s = tab_art.getSelectionModel().getSelectedItem().getDescription();

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Suppression article ");
        alert.setHeaderText("");
        alert.setContentText("Voulez vous supprimer cet article ?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            serv.supprimer(S.getId());
            Aff();
        } else {
            Aff();
        }

    }

    /*@FXML
    private void modifierArt(MouseEvent event) {
    
    article S = (article) tab_art.getSelectionModel().getSelectedItem();
    int i=tab_art.getSelectionModel().getSelectedItem().getId();
    S.setId(i);
    S.setDescription(desc_col.getText());
    S.setDescription(prix_col.getText());
    S.setDescription(date_col.getText());
    S.setDescription(img_col.getText());
    S.setDescription(real_col.getText());
    
    ServiceArticle sa= new ServiceArticle();
    sa.modifier(S);
    Aff();
    
    
    }*/
    @FXML
    private void modifierArt(MouseEvent event) throws IOException {
        modif.setVisible(true);
        ajout.setVisible(false);

        article gets = tab_art.getSelectionModel().getSelectedItem();

        desc.setText("" + gets.getDescription());
        prix.setText("" + gets.getPrix());

        date_col5.setValue(gets.getDate().toLocalDate());
        img.setText("" + gets.getImage());
        real.setText("" + gets.getRealisateur());

    }

    @FXML
    private void modifier(MouseEvent event) throws ParseException {

        article a = (article) tab_art.getSelectionModel().getSelectedItem();
        int i = tab_art.getSelectionModel().getSelectedItem().getId();
        // a.setId(i);
        double pri = Double.parseDouble(prix.getText());
        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date1 = formatter1.parse(date_col5.getValue().toString());
        java.sql.Date d = new java.sql.Date(date1.getTime());
        
        LocalDate today = LocalDate.now();
        int p = d.toLocalDate().
                compareTo(today);
        
        //int h = date1.getValue().compareTo(today);
        String err = "";
        if (desc.getText().length() == 0) {
            err += "\n le champ description est vide";
            desc.setStyle("-fx-border-color : red");
        }
        if (prix.getText().length() == 0) {
        err += "\n le champ prix est vide";
        prix.setStyle("-fx-border-color : red");
        }
        if ((p>0) ) {
        err += "\n le champ date    est vide";
        date_col5.setStyle("-fx-border-color : red");
        }
        if (real.getText().length() == 0) {
            err += "\n le champ realisateur est vide";
            real.setStyle("-fx-border-color : red");
        }
        
        
        if (err != "") {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ajout  Produit");
            alert.setHeaderText(null);
            alert.setContentText(err);
            alert.showAndWait();
            err = "";
            
            
        }else{
        

        a.setDescription(desc.getText());
        a.setPrix(pri);
        a.setDate(d);
        a.setRealisateur(real.getText());
        a.setImage(img.getText());

        ServiceArticle serv = new ServiceArticle();
        serv.modifier(a);
        }
        Aff();
         
    }

    @FXML
    private void search(KeyEvent event) {
        //  col_nom_sujet.setCellValueFactory(new PropertyValueFactory<>("nom_sujet"));
        desc_col.setCellValueFactory(new PropertyValueFactory<>("description"));

        ObservableList<article> oblist = FXCollections.observableArrayList();

        try {
            ResultSet rs = C.createStatement().executeQuery("SELECT * FROM article WHERE description LIKE  '%" + recherche.getText() + "%'"
            //    + " UNION SELECT * FROM sujet WHERE description LIKE '%" + rech.getText() + "%'"
            //  + " UNION SELECT * FROM sujet WHERE nom_categorie_sujet LIKE '%" + rech.getText() + "%'"
            //  + " UNION SELECT * FROM sujet WHERE type LIKE '%" + rech.getText() + "%'"
            );

            while (rs.next()) {

                oblist.add(new article(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getDate(4), rs.getString(5), rs.getString(6)));

                // System.out.println("  " +rs.getString(1)+"  "+rs.getString(2)+"  "+rs.getString(3)+"  "+rs.getString(4));
            }

        } catch (SQLException ex) {
            System.err.println("ERROR" + ex);

        }
        tab_art.setItems(oblist);
    }

    @FXML
    private void annuler_m(MouseEvent event) {

        modif.setVisible(false);
        ajout.setVisible(true);
    }

    @FXML
    private void ajouter(MouseEvent event) throws ParseException {
         SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date1 = formatter1.parse(dateField.getValue().toString());
        java.sql.Date d = new java.sql.Date(date1.getTime());
        System.out.println(d);
        LocalDate today = LocalDate.now();
        int p = d.toLocalDate().
                compareTo(today);
        
        //int h = date1.getValue().compareTo(today);
        String err = "";
        if (desc1.getText().length() == 0) {
            err += "\n le champ description est vide";
            desc1.setStyle("-fx-border-color : red");
        }
        if (prix1.getText().length() == 0) {
        err += "\n le champ prix est vide";
        prix1.setStyle("-fx-border-color : red");
        }
        if ((p>0) ) {
        err += "\n le champ date    est vide";
        dateField.setStyle("-fx-border-color : red");
        }
        if (real1.getText().length() == 0) {
            err += "\n le champ realisateur est vide";
            real1.setStyle("-fx-border-color : red");
        }
        
        
        if (err != "") {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ajout  Produit");
            alert.setHeaderText(null);
            alert.setContentText(err);
            alert.showAndWait();
            err = "";
            
            
        }else{   
        double pri = Double.parseDouble(prix1.getText());
        
        article a = new article(desc1.getText(), pri, d, imageee, real1.getText());
        ServiceArticle sa = new ServiceArticle();
        sa.ajouter(a);
        desc1.clear();
        desc1.setStyle("-fx-border-color : green");
        prix1.setStyle("-fx-border-color : green");
        dateField.setStyle("-fx-border-color : green");
        real1.setStyle("-fx-border-color : green");
        prix1.clear();
        chemin.setText("");
        dateField.setValue(null);
        // dateField.getEditor().clear();
        //img1.clear();
        real1.clear();
        
        Notifications notificationBuilder = Notifications.create().title("Notification").text("ajout article avec succés").hideAfter(Duration.seconds(10)).position(Pos.BOTTOM_RIGHT).onAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
        System.out.println("clicked");
        }
        });
        notificationBuilder.darkStyle();
        notificationBuilder.show();
        }
        Aff();

    }
    @FXML
    private void OneditChanging(TableColumn.CellEditEvent<article, String> event) {
        article article = tab_art.getSelectionModel().getSelectedItem();
        article.setDescription(event.getNewValue());
        ServiceArticle serv = new ServiceArticle();
        serv.modifier(article);
        Aff();

    }

    //double pri = Float.parseFloat(prix.getText());
    // Date d;
    // desc_col.setCellValueFactory(TextFieldTableCell.forTableColumn());
    //   desc_col.setOnEditCommit(e->
    // {e.getTableView().getItems().get(e.getTablePosition().getRow()).setDescription(e.getNewValue());
    //});
    @FXML
    private void importerImage(MouseEvent event) {

        final FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll( // new FileChooser.ExtensionFilter("Image", ".jpg", ".png")
                );
        File file = fileChooser.showOpenDialog(null);
        if (file != null) {

            File currDir = new File(System.getProperty("user.dir", "."));
            System.out.println(currDir.toPath().getRoot().toString());

            String path = currDir.toPath().getRoot().toString() + "C:\\wamp64\\www\\pidevsymfony1\\web\\uploads\\images";
            ImageService u = new ImageService();
            try {
                u.upload(file, path);
            } catch (IOException ex) {

            }
            imageee = file.getName();
            img.setText(imageee);

        } else {
            System.out.println("FICHIER erroné");
        }
    }

    
    
    @FXML
    private void consulter(ActionEvent event) {
        
         try {
                            Parent root = FXMLLoader.load(getClass().getResource("/interfaces/listEval.fxml"));
                            Scene scene = new Scene(root);
                            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                            app_stage.setScene(scene);
                            app_stage.show();
                        } catch (IOException ex) {
                        }
    }

}
