/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import entities.article;
import java.net.URL;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import services.ServiceArticle;

/**
 * FXML Controller class
 *
 * @author user
 */
public class AjoutArtController implements Initializable {

    @FXML
    private TextField prix;
    @FXML
    private TextField desc;
    @FXML
    private Button boutonModifier;
    @FXML
    private AnchorPane notification;
    @FXML
    private Label label;
    @FXML
    private TextField real;
    @FXML
    private TextField img;
    @FXML
    private DatePicker dateField;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        // TODO
    }    

    @FXML
    private void ajouter(MouseEvent event) throws ParseException {
       double pri = Double.parseDouble(prix.getText());
        
        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date1 = formatter1.parse(dateField.getValue().toString());
       java.sql.Date d= new java.sql.Date(date1.getTime());
       System.out.println(d);
       article a= new article(desc.getText(),pri,d, img.getText(), real.getText());
        
        ServiceArticle sa= new ServiceArticle();
        sa.ajouter(a);
      
        
         
    }
    
}
