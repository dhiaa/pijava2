/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import entities.article;
import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import services.ServiceArticle;

/**
 * FXML Controller class
 *
 * @author user
 */
public class ModifierArtController implements Initializable {

    @FXML
    private TextField prix;
    @FXML
    private TextField desc;
    @FXML
    private Button boutonModifier;
    @FXML
    private AnchorPane notification;
    @FXML
    private Label label;
    @FXML
    private TextField real;
    @FXML
    private TextField img;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void modifier(MouseEvent event) {
        
         double pri = Float.parseFloat(prix.getText());
        Date d;
        
                article a= new article(desc.getText(),pri,null, img.getText(), real.getText());
        
        ServiceArticle sa= new ServiceArticle();
        sa.modifier(a);
        
    }
    
}
