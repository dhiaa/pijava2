/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import entities.Evaluation;
import entities.Favoris_client;
import entities.article;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
//import javafx.scene.control.ListCell;
//import javafx.scene.control.ListCell;

import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.controlsfx.control.Rating;
import services.ServiceArticle;
import services.ServiceEvaluation;
import services.ServiceFavoris;

/**
 * FXML Controller class
 *
 * @author user
 */
public class ProduitsController implements Initializable {

    @FXML
    private ListView<article> listview1;
    ObservableList<article> data;
    @FXML
    private Label lbltitre;
    @FXML
    private ImageView lbl_img;
    @FXML
    private Label cheminimage;
    @FXML
    private Label realisa;
    @FXML
    private Label dateee;
    @FXML
    private Button evaluer;
    @FXML
    private Label id_produit;
    @FXML
    private Rating rating;
    @FXML
    private Label nomLabel;
    @FXML
    private Label dateLabel;
    @FXML
    private Label realLabel;
    @FXML
    private Label moyenne;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        data = FXCollections.observableArrayList();
        setcellValue();
        loadDataFromDatabase();
        listview1.setCellFactory(lv -> new Poules());
        nomLabel.setVisible(false);
        dateLabel.setVisible(false);
        realLabel.setVisible(false);
    }

    @FXML
    private void Evaluer(ActionEvent event) throws SQLException {
        ServiceEvaluation serv = new ServiceEvaluation();
        String s = "non";
        int id;
        List<Evaluation> listeEval = new ArrayList<Evaluation>();
        listeEval = serv.findAll();
        for (Evaluation e : listeEval) {
            if (e.getIdprod() == Integer.valueOf(id_produit.getText())) {  //   if produit evoluer
                s = "oui";
                id = e.getIdevaluation();
                
            }
        }
        Evaluation v = new Evaluation();
        //v.setIduser(uss.getId());
        v.setIduser(1);
        v.setIdprod(Integer.valueOf(id_produit.getText()));
        v.setNote(rating.getRating());
        if (s.equals("non")) { //premiere evaluation
            serv.ajouterEvaluation(v);
            //rating.setRating(0);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("AJOUT evaluation");
            alert.setHeaderText(null);
            alert.setContentText("ajout evaluation établie avec succès");
            alert.showAndWait();
        } else {
           // ServiceEvaluation crud = new ServiceEvaluation();
            Evaluation e = serv.findByIdProd(Integer.valueOf(id_produit.getText()));
            e.setNote(rating.getRating());
            serv.modifierreclamation(e);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("MODIFICATION evaluation");
            alert.setHeaderText(null);
            alert.setContentText("modification evaluation établie avec succès");
            alert.showAndWait();
            
        }
        data.clear();
        loadDataFromDatabase();

    }

    @FXML
    private void listaa(javafx.scene.input.MouseEvent event) {
        try {
                            Parent root = FXMLLoader.load(getClass().getResource("/interfaces/listFavoris.fxml"));
                            Scene scene = new Scene(root);
                            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                            app_stage.setScene(scene);
                            app_stage.show();
                        } catch (IOException ex) {
                        }
    }

    static public class Poules extends ListCell<article> {

        public Poules() {

        }

        protected void updateItem(article item, boolean bln) {
            super.updateItem(item, bln);

            if (item != null) {

                Text t5 = new Text(item.getDescription());
                Text t3 = new Text(item.getPrix() + "  dt");
                Button btn = new Button("ajouter au panier");
                // btn.setOnAction(value);
                btn.setOnAction(new javafx.event.EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try {
                            Parent root = FXMLLoader.load(getClass().getResource("/interfaces/affAllArt.fxml"));
                            Scene scene = new Scene(root);
                            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                            app_stage.setScene(scene);
                            app_stage.show();
                        } catch (IOException ex) {
                        }

                    }
                });
                Button  addtofav=new Button();
              // ImageView iv = new ImageView("file:/wamp64/www/pidevsymfony1/web/uploads/images/favorite.png");
               ImageView iv = new ImageView("file:/Users/user/Documents/GitHub/pijava/pijava3/src/ressources/image/favoris.png");
                iv.setFitWidth(35);
                iv.setPreserveRatio(true);
                iv.setSmooth(true);
                iv.setCache(true);
                addtofav.setGraphic(iv);
                addtofav.setStyle("-fx-cursor : hand;");
                addtofav.setOnAction(new javafx.event.EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                    ServiceFavoris sf=new ServiceFavoris();
                        Favoris_client f=new Favoris_client(item,2);
                    sf.Ajouter_favoris(item.getId(), f);
             }
     } );
                

                t3.setStyle("-fx-font-size: 20 arial;");
                t5.setStyle("-fx-font-size: 20 arial;");
                VBox vBox = new VBox(t5, t3, btn,addtofav);
                vBox.setSpacing(40);
                //File file = new File("C:\\wamp64\\www\\DevSpace\\web\\devis", item.getDevis_name1());

                // Image image = new Image(file.toURI().toString());
                //ImageView img = new ImageView(image);
                //img.setFitHeight(100);
                //img.setFitWidth(80);
                File file = new File("C:\\wamp64\\www\\pidevsymfony1\\web\\uploads\\images", item.getImage());

                Image image = new Image(file.toURI().toString());
                ImageView img = new ImageView(image);
                img.setFitHeight(200);
                img.setFitWidth(160);

                HBox hBox = new HBox(img, vBox);

                hBox.setSpacing(10);
                setGraphic(hBox);
            }
        }
    }

    private void loadDataFromDatabase() {
        try {
            ServiceArticle service = new ServiceArticle();

            List<article> rs = service.remplir();
            
            for (article a : rs) {
                article r = new article();
                r.setId(a.getId());
                r.setDescription(a.getDescription());
                r.setPrix(a.getPrix());
                r.setDate(a.getDate());
                r.setImage(a.getImage());
                r.setRealisateur(a.getRealisateur());

                data.add(r);
                //System.out.println("recup table view ok !");

            }

        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
        listview1.setItems(data);

    }

    private void setcellValue() {
        listview1.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>() {
            @Override
            public void handle(javafx.scene.input.MouseEvent event) {
                 rating.setRating(0);

                article a = (article) listview1.getItems().get(listview1.getSelectionModel().getSelectedIndex());
                ServiceEvaluation crud = new ServiceEvaluation();

                lbltitre.setText(a.getDescription());
                realisa.setText(a.getRealisateur());
                dateee.setText(a.getDate().toString());
                id_produit.setText(String.valueOf(a.getId()));
               
               double moy=crud.moyByName(a.getId());
               moyenne.setText(String.valueOf(moy)+"/5");
                
                nomLabel.setVisible(true);
                dateLabel.setVisible(true);
                realLabel.setVisible(true);

                cheminimage.setText(a.getImage());
                Evaluation e = crud.findByIdProd(Integer.valueOf(id_produit.getText()));

                //double ell = crud.findnote(Integer.valueOf(id_produit.getText()));
                 //rating.setRating(e.getNote());


                ServiceEvaluation serv = new ServiceEvaluation();
                String s = "non";
                List<Evaluation> listeEval = new ArrayList<Evaluation>();
                listeEval = serv.findAll();

                   // e.setNote(moy);
                for (Evaluation ee : listeEval) {
                    if (ee.getIdprod() == Integer.valueOf(id_produit.getText())) {
                        s = "oui";
                    }
                
                 }
                if (s.equals("non")) {
                    rating.setRating(0);
                } else {
                    System.out.println(e.getNote());
                    rating.setRating((double)e.getNote());
                    
                   //rating.getRating();
                }
                
                
                //dateee.setText(String.valueOf(a.getPrix()));

                // taillelbl.setText(p.getTaille());
                //cheminImage.setText(p.getImage());
                //marquelbl.setText(p.getMarque());
                //couleurlbl.setText(p.getCouleur());
                //descriptionlbl.setText(p.getDescription());
                // selected=true;
                Image();

            }
        });
    }

    private void Image() {

        File file = new File("C:\\wamp64\\www\\pidevsymfony1\\web\\uploads\\images", cheminimage.getText());
        Image image = new Image(file.toURI().toString());
        lbl_img.setImage(image);

    }

}
