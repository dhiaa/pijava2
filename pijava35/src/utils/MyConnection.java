/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.sql.Connection;
import java.sql.*;


/**
 *
 * @author user
 */
public class MyConnection {
     private  String url="jdbc:mysql://127.0.0.1:3306/pidev";
    private  String user="root";
    private  String password="";
    
    private  Connection conn;
    
    static MyConnection inst;
    
    private MyConnection()
    {
      try {
            //  Class.forName("driver");

            
            conn= DriverManager.getConnection(url , user , password);
                    System.out.println("connected");
     
                    
                    } catch (SQLException ex) {
        } 
    }

    public Connection getConn() {
        return conn;
    }


    
    public static MyConnection getInstance()
    {
    if(inst==null){ inst=new MyConnection();}
    return inst; 
    
    }
    
    
    
   
    
}
