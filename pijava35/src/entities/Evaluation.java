/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author chaym
 */
public class Evaluation {
    private int idevaluation;
    private int idprod;
    private String nomProduit;
    private String username ;
    private int iduser;
    private double note;

    
    
    
    public Evaluation() {
    }

    public Evaluation(int idevaluation, int idprod,double note, int iduser,String nomProduit,String username) {
        this.idevaluation = idevaluation;
        this.idprod = idprod;
        this.iduser = iduser;
        this.note = note;
        this.nomProduit=nomProduit;
        this.username=username;
    }

    public int getIdevaluation() {
        return idevaluation;
    }

    public void setIdevaluation(int idevaluation) {
        this.idevaluation = idevaluation;
    }

    public int getIdprod() {
        return idprod;
    }

    public void setIdprod(int idprod) {
        this.idprod = idprod;
    }



    public int getIduser() {
        return iduser;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

    public double getNote() {
        return note;
    }

    public void setNote(double note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "Evaluation{" + "idevaluation=" + idevaluation + ", idprod=" + idprod + ", iduser=" + iduser + ", note=" + note + '}';
    }

    public String getNomProduit() {
        return nomProduit;
    }

    public void setNomProduit(String nomProduit) {
        this.nomProduit = nomProduit;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    
    
    
    
    
}
