/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.sql.Timestamp;
import java.util.Objects;

/**
 *
 * @author user
 */
public class Commande {
    
    int id,id_user;
    String lieu_liv;
float somme;
Timestamp date_cmd;

    public Commande() {
    }

    public Commande(int id_user, String lieu_liv, float somme, Timestamp date_cmd) {
        this.id_user = id_user;
        this.lieu_liv = lieu_liv;
        this.somme = somme;
        this.date_cmd = date_cmd;
    }
   

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getLieu_liv() {
        return lieu_liv;
    }

    public void setLieu_liv(String lieu_liv) {
        this.lieu_liv = lieu_liv;
    }

    public float getSomme() {
        return somme;
    }

    public void setSomme(float somme) {
        this.somme = somme;
    }

    public Timestamp getDate_cmd() {
        return date_cmd;
    }

    public void setDate_cmd(Timestamp date_cmd) {
        this.date_cmd = date_cmd;
    }

    
}
