/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.sql.Date;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


/**
 *
 * @author user
 */
public class article {
    private int id ;
    private String description ;
    private double prix ;
    private Date date ;
    private String image ;
    private String realisateur ;
    private ImageView  img ;

    public article() {
    }

    

    

    public ImageView getImg() {
        return img;
    }

    public void setImg(ImageView img) {
        this.img = img;
    }

    public article(int id, String description, double prix, Date date,ImageView img, String realisateur ) {
        this.id = id;
        this.description = description;
        this.prix = prix;
        this.date = date;
        this.realisateur = realisateur;
        this.img = img;
    }
    

    
    public article(int id, String description, double prix, Date date, String image, String realisateur) {
        this.id = id;
        this.description = description;
        this.prix = prix;
        this.date = date;
        this.image = image;
        this.realisateur = realisateur;
    }

    public article(String description, double prix, Date date, String image, String realisateur) {
        this.description = description;
        this.prix = prix;
        this.date = date;
        this.image = image;
        this.realisateur = realisateur;
    }

    public article(String description) {
        this.description = description;
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRealisateur() {
        return realisateur;
    }

    public void setRealisateur(String realisateur) {
        this.realisateur = realisateur;
    }

    @Override
    public String toString() {
        return "article{" + "id=" + id + ", description=" + description + ", prix=" + prix + ", date=" + date + ", image=" + image + ", realisateur=" + realisateur + '}';
    }

   

    

    
    
}
