/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.article;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import utils.MyConnection;

/**
 *
 * @author user
 */
public class ServiceArticle {

    Connection c = MyConnection.getInstance().getConn();

    public void ajouter(article a) {

        try {
            Statement st = c.createStatement();
            String req = "insert into article (description,prix,date,image,realisateur) values ('" + a.getDescription() + "'," + a.getPrix() + ",'" + a.getDate() + "','" + a.getImage() + "','" + a.getRealisateur() + "')";

            st.executeUpdate(req);
            System.out.println("worked");

        } catch (SQLException ex) {
            System.out.println("dadadadada");
            System.out.println(ex);

        }

    }

    public void modifier(article a) {
        try {
            PreparedStatement pt = c.prepareStatement("update article set description = ?,prix=?,date=?,image=?,realisateur=? where id= ?");
            pt.setInt(6, a.getId());

            pt.setString(1, a.getDescription());
            pt.setDouble(2, a.getPrix());
            pt.setDate(3, a.getDate());
            pt.setString(4, a.getImage());
            pt.setString(5, a.getRealisateur());

            pt.executeUpdate();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Modifier un sujet");
            alert.setHeaderText(null);
            alert.setContentText("modification du sujet établie avec succès");
            alert.showAndWait();
        } catch (SQLException ex) {
            System.out.println(" modification failed");

        }

    }

    public void supprimer(int id) {
        try {
            PreparedStatement pt = c.prepareStatement("delete from article where id= ? ");

            pt.setInt(1, id);
            pt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(" delete failed");

        }

    }

    public void afficher() {
        try {
            Statement st = c.createStatement();
            String req = "select*from article";
            
            ResultSet rs = st.executeQuery(req);
         //Image image1 = new Image("file:/wamp64/www/fixit/web/uploads/images/categorieProduit/"+rs.getString(4), 120, 120, false, false);

            System.out.println("service:afficher()" + req);
            
            while (rs.next()) {
                System.out.println(rs.getInt(1) + rs.getString(2) + rs.getDouble(3) + rs.getDate(4) + rs.getString(5) + rs.getString(6));
            }
        } catch (SQLException ex) {

            System.out.println("failed");

        }

    }
    public ObservableList<article> afficherImage() throws SQLException {

        Statement st = c.createStatement();
        String req = "select*from article";
       
        ObservableList<article> la = FXCollections.observableArrayList();


        ResultSet rs = st.executeQuery(req);
        System.out.println("service:afficher()" + req);
        while (rs.next() == true) {
                    Image image1 = new Image("file:/wamp64/www/pidevsymfony1/web/uploads/images/"+rs.getString(5), 60, 60, false, false);
                    ImageView im=new ImageView(image1);
            article a = new article();
            a.setId(rs.getInt("id"));
            a.setDescription(rs.getString("description"));
            a.setPrix(rs.getDouble("prix"));
            a.setDate(rs.getDate("date"));
            a.setImage(rs.getString("image"));
            a.setRealisateur(rs.getString("realisateur"));
            a.setImg(im);

            la.add(a);
            System.out.println(a.toString());
        }
        //System.out.println(la.toString());
        return la ;
        
        }

    public List<article> remplir() throws SQLException {

        Statement st = c.createStatement();
        String req = "select*from article";
       
        List<article> la = new ArrayList<article>();


        ResultSet rs = st.executeQuery(req);
        System.out.println("service:afficher()" + req);

        /*while (rs.next()) {
            System.out.println(rs.getInt(1) + rs.getString(2) + rs.getDouble(3) + rs.getDate(4) + rs.getString(5) + rs.getString(6));
            }*/
        while (rs.next() == true) {
            article a = new article();
            a.setId(rs.getInt("id"));
            a.setDescription(rs.getString("description"));
            a.setPrix(rs.getDouble("prix"));
            a.setDate(rs.getDate("date"));
            a.setImage(rs.getString("image"));
            a.setRealisateur(rs.getString("realisateur"));
            //a.setImg(rs.g);
          Image image1 = new Image("file:/wamp64/www/fixit/web/uploads/images/categorieProduit/"+rs.getString(4), 120, 120, false, false);
          a.setImg(new ImageView(image1));

            la.add(a);
           // System.out.println(a.toString());
        }
        //System.out.println(la.toString());
        return la ;
        
        }
    
    /* @FXML
    private void importerImage(ActionEvent event) {
    final FileChooser fileChooser = new FileChooser();
    fileChooser.getExtensionFilters().addAll(
    new FileChooser.ExtensionFilter("Image", ".jpg", ".png")
    );
    File file = fileChooser.showOpenDialog(null);
    if (file != null) {
    
    File currDir = new File(System.getProperty("user.dir", "."));
    System.out.println(currDir.toPath().getRoot().toString());
    
    String path = currDir.toPath().getRoot().toString() + "wamp64/www/fixit/web/uploads/images/produit/";
    ImageService u = new ImageService();
    try {
    u.upload(file, path);
    } catch (IOException ex) {
    
    }
    imageee = file.getName();
    } else {
    System.out.println("FICHIER erroné");
    }
    }*/
  

    }

