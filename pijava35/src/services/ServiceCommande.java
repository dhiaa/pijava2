/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Commande;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import utils.MyConnection;

/**
 *
 * @author user
 */
public class ServiceCommande {
        Connection c = MyConnection.getInstance().getConn();
        Statement stm;

    public void AjouterCmd(Commande cmd) {
       
        try {
        String req_conn="insert into panier(somme,id_user,lieu_liv)"
                +   " VALUES (?,?,?)";
       
  PreparedStatement prt = c.prepareStatement(req_conn);
            
           
            prt.setDouble(1, cmd.getSomme());   
            prt.setInt(2, cmd.getId_user());
            prt.setString(4, cmd.getLieu_liv());   
            //prt.setDate(5, cmd.getDate_cmd());      
            System.out.println("****"+prt);
            prt.executeUpdate();
               System.out.println("****"+prt);
            {JOptionPane.showMessageDialog(null,"Commande Ajouter en succes");}
    } catch (SQLException s) {
        {JOptionPane.showMessageDialog(null,"Erreur!");}
        System.out.println("Erreur dans AjouterCmd"+s);
    }
    }
    
    public void SupprimerCmd(int i) {
         try {
        String req_conn="delete from commande where id=?";
       
  PreparedStatement prt = c.prepareStatement(req_conn);
            
            prt.setInt(1,i);
            prt.executeUpdate();
            {JOptionPane.showMessageDialog(null,"Commande annuler");}
    } catch (SQLException s) {
        {JOptionPane.showMessageDialog(null,"Verifier votre information!");}
        System.out.println("Erreur dans Supprim Commande");
    }
    }

    public void ModifierCmd(Commande cmd) {
                try {
        String req_conn="update panier set lieu_liv=? ,somme=? where id="+cmd.getId();
                
       
  PreparedStatement prt = c.prepareStatement(req_conn);
            
              
            prt.setDouble(2, cmd.getSomme());
            prt.setString(4, cmd.getLieu_liv());
            prt.executeUpdate();
                    System.out.println("***"+prt);
            {JOptionPane.showMessageDialog(null,"Modifier en succes");}
    } catch (SQLException s) {
        {JOptionPane.showMessageDialog(null,"Erreur!");}
        System.out.println("Erreur dans ModifierCmd");
    }
    }
    
    public void AfficherCmd(Commande cmd) {
         
    try {
        
        String req_conn="select * from commande ";
        //PreparedStatement prt = conn.prepareStatement(req_conn);
        ResultSet rs = stm.executeQuery(req_conn);
        while (rs.next())
        {//int id_cmd, String etat_cmd, String mode_p, String lieu_liv, float montant
            System.out.println("commande id: "+rs.getInt(1)
                                +" somme: "+ rs.getDouble(2)
                                +" id user: "+rs.getInt(3)
                                +" lieu livraison: "+rs.getString(4)
                                +" date commande: "+rs.getString(5)
                                
                                );
        }
    } catch (SQLException ex) {
        System.out.println("erreur dafichage du commande"+ ex);
    }
 }
}
