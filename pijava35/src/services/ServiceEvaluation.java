/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.article;
import entities.Evaluation;
import entities.user;
import utils.MyConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Lazzem
 */
public class ServiceEvaluation {

    Connection connection= MyConnection.getInstance().getConn();
    private static ResultSet r;
    private PreparedStatement ps;

    public ServiceEvaluation() {
    
    }

    public void ajouterEvaluation(Evaluation v)  {
                    System.out.println(v.getIduser());

        try
        {
            String req = "insert into evaluation (iduser,idprod,note) values (?,?,?)";
        ps = connection.prepareStatement(req);
        ps.setInt(1,v.getIduser());
        ps.setInt(2, v.getIdprod());
        ps.setDouble(3, v.getNote());
        System.out.println("evaluation ajouté succes");
         
        ps.executeUpdate();
       
        }
        catch(SQLException ex)
        {
            System.out.println(ex);
        }
        
    }

    public String getEvalByIduser(int id) throws SQLException {
        List<Evaluation> listeEvaluation = new ArrayList<>();
        String requete = "select * from evaluation ";
        try {
            Statement statement = connection.createStatement();
            r = statement.executeQuery(requete);
            Evaluation f = new Evaluation();
            while (r.next()) {
                f.setIdevaluation(r.getInt(1));
                f.setIduser(r.getInt(2));
                f.setIdprod(r.getInt(3));
                f.setNote(r.getDouble(4));
            }
            if ((f.getIduser() == 1) && (f.getIdprod()== id)) {
                listeEvaluation.add(f);
            }
            System.out.println("recupération de id evenement " + listeEvaluation.toString());
            for (Evaluation v : listeEvaluation) {
                if (v.getIdevaluation() != 0) {
                    return "existe";
                } else {
                    return "non";
                }
            }

        } catch (SQLException ex) {
            System.out.println("SQL Error: " + ex);
        }
        return null;
    }

    public double moyByName(int id) {
          String requete = "SELECT AVG(note) FROM evaluation WHERE idprod ="+id;
         Double t=0.0 ;
        try {
            Statement statement = connection.prepareStatement(requete);
                        r = statement.executeQuery(requete);

                        while (r.next()) {
                            System.out.println(r.getInt("AVG(note)"));
                         return r.getInt("AVG(note)");
                        }
                        
            } catch (SQLException ex) {
                System.out.println("erreur"+ex);
            
        }
        return -1 ;
          
    }
    public double moyByName1(int id) {
          String requete = "SELECT AVG(note) FROM evaluation WHERE idprod ="+id;
         Double t=0.0 ;
        try {
            Statement statement = connection.prepareStatement(requete);
                        r = statement.executeQuery(requete);

                        while (r.next()) {
                            System.out.println(r.getInt("AVG(note)"));
                         return r.getInt("AVG(note)");
                        }
                        
            } catch (SQLException ex) {
                System.out.println("erreur"+ex);
            
        }
        return -1 ;
          
    }

    public ObservableList<Evaluation> findAll() {
            ObservableList<Evaluation> oblist = FXCollections.observableArrayList();
            



        String requete = "select * from evaluation";
        try {
            Statement statement = connection.prepareStatement(requete);
            r = statement.executeQuery(requete);
            while (r.next()) {
                Evaluation f = new Evaluation();
                f.setIduser(r.getInt("iduser"));
                f.setIdprod(r.getInt("idprod"));
                f.setIdevaluation(r.getInt("idevaluation"));
                f.setNote(r.getDouble("note"));
                article a = this.getarticle(r.getInt(2));
                user uu = this.getuser(r.getInt(4));
                f.setNomProduit(a.getDescription());
                f.setUsername(uu.getUsername());
                //System.out.println("************************************************************************* FIND ALL");

                System.out.println(f.toString());
                oblist.add(f);
            }
            return oblist;
        } catch (SQLException ex) {
            System.out.println("SQL Error: ****************************************************************" + ex);
            return null;
        }
    }
      public ArrayList<Double> findAll5() {
            //ObservableList<Evaluation> oblist = FXCollections.observableArrayList();
                   List<Double> listmoy = new ArrayList<>();

        String requete = "select * from evaluation";
        try {
            Statement statement = connection.prepareStatement(requete);
            r = statement.executeQuery(requete);
            while (r.next()) {
                
                System.out.println("***********************************************************************dhia*****************************************");
                System.out.println(r.getInt(2)+"***********************************************************************dhia*****************************************");
                //System.out.println(moyByName(r.getInt(1)));
                //listmoy.add(moyByName(r.getInt(2)));
                 /*System.out.println(listmoy.size());
                 
                 for (Double e : listmoy) {  
                    System.out.println(e+"********************************************");
                   }*/

               
                
            }
            return  (ArrayList<Double>) listmoy;
        } catch (SQLException ex) {
            System.out.println("SQL Error: *********************************************" + ex);
            return null ;
        }
    }
    

    public void modifierreclamation(Evaluation rec) {
        try {
            String req;
           // req = "UPDATE evaluation SET iduser=?,idprod=?,note=? WHERE idevaluation=?";
            req = "UPDATE evaluation SET note=? WHERE idevaluation=?";
            ps = connection.prepareStatement(req);
            //ps.setInt(1, rec.getIduser());
            //ps.setInt(2, rec.getIdprod());
            ps.setDouble(1, rec.getNote());
            ps.setInt(2, rec.getIdevaluation());
            ps.executeUpdate();
                        System.out.println("evaluation modifier**********************************************************************");

        } catch (SQLException ex) {
            System.out.println(ps);
            System.out.println("modifier note failed********************************************************************************************");
            System.out.println(ex.getMessage());
        }

    }

    public Evaluation findByIdProd(Integer id) {
        String req = "select * from evaluation where idprod = ? and iduser=1";
        
        Evaluation eva = null;
        try {
            ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                article a = this.getarticle(resultSet.getInt(2));
                user u1 = this.getuser(resultSet.getInt(4));
                eva = new Evaluation(resultSet.getInt(1), resultSet.getInt(2), resultSet.getDouble(3),resultSet.getInt(4),a.getDescription(),u1.getUsername());
                System.out.println("Services.UserService.findById()");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return eva;
    }
    public double findnote(Integer id) {
        String req = "select note from evaluation where idprod = ? and iduser=1";
        double notee = 0;
        try {
            ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                notee=  resultSet.getFloat(req) ;
                //eva = new Evaluation(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3), resultSet.getDouble(4));
                System.out.println("Services.UserService.findById()");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return notee;
    }
    public article getarticle(int Id) {
        try {
            PreparedStatement pt = connection.prepareStatement("SELECT * from article where id=?");
            pt.setInt(1, Id);
            ResultSet rs = pt.executeQuery();
            while (rs.next()) {
                article a = new article();
                Image image1 = new Image("file:/wamp64/www/pidevsymfony1/web/uploads/images/"+rs.getString(5), 60, 60, false, false);
                    ImageView im=new ImageView(image1);
            a.setId(rs.getInt("id"));
            a.setDescription(rs.getString("description"));
            a.setPrix(rs.getDouble("prix"));
            a.setDate(rs.getDate("date"));
            a.setImage(rs.getString("image"));
            a.setRealisateur(rs.getString("realisateur"));
            a.setImg(im);
                return a;
            }
        } catch (SQLException ex) {

            System.out.println("Services.OutilService.getCategorieOutil()");
            System.out.println(ex);
        }
        return null;
    }
    public user getuser(int Id) {
        try {
            PreparedStatement pt = connection.prepareStatement("SELECT * from user where id=?");
            pt.setInt(1, Id);
            ResultSet rs = pt.executeQuery();
            while (rs.next()) {
                user a = new user();
                
            a.setId(rs.getInt("id"));
            a.setUsername(rs.getString("username"));
            a.setUsername_canonical(rs.getString("Username_canonical"));
            a.setEmail(rs.getString("email"));
            a.setEmail_canonical(rs.getString("email_canonical"));
            a.setEnabled(rs.getInt("enabled"));
            a.setSalt(rs.getString("salt"));
            a.setPassword(rs.getString("password"));
            a.setConfirmation_token(rs.getString("confirmation_token"));
            a.setRoles(rs.getString("roles"));
            a.setNom(rs.getString("nom"));
            a.setPrenom(rs.getString("prenom"));
            a.setPassword_requested_at(rs.getDate("password_requested_at"));
            a.setLast_login(rs.getDate("last_login"));
            
                return a;
            }
        } catch (SQLException ex) {

            System.out.println("Services.OutilService.getCategorieOutil()");
            System.out.println(ex);
        }
        return null;
    }
    
   public double moyByName5(int id) {
        double sum = 0;
        double moy = 0;
        ServiceEvaluation se = new ServiceEvaluation();
        List<Evaluation> eqs = new ArrayList<>();
        eqs = se.findAll().stream().filter(e -> e.getIdprod()== id).collect(Collectors.toList());
        /* System.out.println(se.findAll().stream()
        .filter(s -> s.getNote())
        .skip(1)
        .
        .findFirst()
        .toString());*/
        /*      se.findAll().stream()
        .filter(s->s.getIdprod() == id)
        .sorted((s1,s2)->s1.getNote() - s2.getNote())
        .forEach(s->System.out.println(s));*/
        for (Evaluation e : eqs) {
            sum = sum + e.getNote();
        }
        moy = sum / eqs.size();
        System.out.println("recuperation by id" + moy);
        return moy = sum / eqs.size();
    }
   
   public ArrayList findAllmoy() {
            ArrayList oblist =( ArrayList ) FXCollections.observableArrayList();
            



        String requete = "select * from evaluation";
        try {
            Statement statement = connection.prepareStatement(requete);
            r = statement.executeQuery(requete);
            while (r.next()) {
                Evaluation f = new Evaluation();
                f.setIduser(r.getInt("iduser"));
                f.setIdprod(r.getInt("idprod"));
                f.setIdevaluation(r.getInt("idevaluation"));
                f.setNote(r.getDouble("note"));
                article a = this.getarticle(r.getInt(2));
                user uu = this.getuser(r.getInt(4));
                f.setNomProduit(a.getDescription());
                f.setUsername(uu.getUsername());
                //System.out.println("************************************************************************* FIND ALL");

                System.out.println(f.toString());
                oblist.add(f);
            }
            return oblist;
        } catch (SQLException ex) {
            System.out.println("SQL Error: ****************************************************************" + ex);
            return null;
        }
    }
   
    public ObservableList<article> top5nomart()
    {
        ObservableList<article> oblist = FXCollections.observableArrayList();
         String requete = "select AVG(note) avg ,idprod FROM evaluation GROUP BY idprod ORDER BY avg DESC LIMIT 5";
        try {
            Statement statement = connection.prepareStatement(requete);
            r = statement.executeQuery(requete);
            while (r.next()) {
                article a = this.getarticle(r.getInt("idprod"));
                r.getInt(1);
                
               
                System.out.println(a.getDescription());
                oblist.add(a);
                //System.out.println("************************************************************************* FIND ALL");

                
            }
           
            return oblist;
        } catch (SQLException ex) {
            System.out.println("SQL Error: ****************************************************************" + ex);
            return null;
        }
    }
    public ObservableList<Double> top5moy()
    {
        ObservableList<Double> oblist = FXCollections.observableArrayList();
         String requete = "select AVG(note) avg ,idprod FROM evaluation GROUP BY idprod ORDER BY avg DESC LIMIT 5";
        try {
            Statement statement = connection.prepareStatement(requete);
            r = statement.executeQuery(requete);
            while (r.next()) {
                
                r.getDouble(1);
                
               
                
                oblist.add(r.getDouble(1));
                System.out.println(r.getDouble(1));
                //System.out.println("************************************************************************* FIND ALL");

                
            }
           
            return oblist;
        } catch (SQLException ex) {
            System.out.println("SQL Error: ****************************************************************" + ex);
            return null;
        }
    }
    
}
