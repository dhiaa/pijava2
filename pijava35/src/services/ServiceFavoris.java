/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Favoris_client;
import entities.article;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javax.swing.JOptionPane;
import utils.MyConnection;

/**
 *
 * @author user
 */
public class ServiceFavoris {

    Connection c = MyConnection.getInstance().getConn();

    public boolean verifexistance(int art_id) throws SQLException {
        ResultSet rs1 = c.createStatement().executeQuery("SELECT * FROM favoris where iduser=" + 1 + " and idprod=" + art_id);
        boolean existe = true;
        while (rs1.next()) {
            existe = false;
        }
        return existe;
    }

    public List<String> get_tel(int id_p) {
        List<String> str = new ArrayList<>();
        {
            String n;

            int i = 0;
            try {
                Statement st1 = c.createStatement();
                String req1 = "select id_client from favoris where id_produit= " + id_p;//les users elli 3andhom produit hedha fel favoris
                ResultSet rs1 = st1.executeQuery(req1);

                while (rs1.next()) {
                    i = rs1.getInt(1);

                    Statement st = c.createStatement();
                    String req = "select telephone from user where id_user= " + i;
                    ResultSet rs = st.executeQuery(req);

                    while (rs.next()) {
                        n = rs.getString(1);
                        str.add(n);
                    }
                }
            } catch (SQLException e) {
                System.out.println("errrr " + e.getMessage());
            }
            return str;
        }
    }

    public int get_id_user() {
        int id_user = 1;
        //try {   
        //Statement st1=c.createStatement();
        //String req1="select * from user where connected= "+true;
        //ResultSet rs1=st1.executeQuery(req1);

        //while(rs1.next()){
        //     id_user=rs1.getInt(1);}         
        //   } catch (SQLException e) {
        // System.out.println("errrr "+ e.getMessage());
        //} 
        return id_user;
    }

    public article get_prod(int i) {
        article a = null;
        try {
            Statement st1 = c.createStatement();
            String req1 = "select * from Produit where id_produit= " + i;
            ResultSet rs = st1.executeQuery(req1);

            while (rs.next()) {
                a = new article(rs.getInt(1) + rs.getString(2) + rs.getDouble(3) + rs.getDate(4) + rs.getString(5) + rs.getString(6));
            }
        } catch (SQLException e) {
            System.out.println("errrr " + e.getMessage());
        }
        return a;
    }

    /* public String get_nom_pep(article a)
   {   String n="";
   try {
   Statement st1=c.createStatement();
   String req1="select nom_pepiniere from user where id_user= "+.getId_user();
   ResultSet rs1=st1.executeQuery(req1);
   
   while(rs1.next()){
   n=rs1.getString(1);}
   } catch (SQLException e) {
   System.out.println("errrr "+ e.getMessage());
   }
   return n;
   } */
    public void Ajouter_favoris(int p, Favoris_client f) {

        try {
            Statement st1 = c.createStatement();
            String req1 = "select description,realisateur from article where"  // recuperation description , realisateur
                    + " id= " + p; 
            System.out.println("************************************* ajouter favoris");
            ResultSet rs1 = st1.executeQuery(req1);
            String desc = "";
            String real = "";
            
            while (rs1.next()) {
                desc = rs1.getString(1);
                real = rs1.getString(2);
            }

            /* Statement st2 = c.createStatement();
            String req2 = "select role from user where id_user= "
            + get_id_user();
            ResultSet rs2 = st2.executeQuery(req2);
            String role_cl = "";
            
             while (rs2.next()) {
            role_cl = rs2.getString(1);
            }*/
            /*boolean dispo = false;
            if (qte > 0) {
            dispo = true;
            }*/
            if (verifexistance(p)) {
                Statement st = c.createStatement();
                String req = "insert into favoris values("
                        + f.getId_fav() + "," + p
                        + "," + get_id_user() + ",'" + desc
                        + "','" + real + "')";
                JOptionPane.showMessageDialog(null, "ajout produit dans liste des favoris! ");

                System.out.println("doneeeeee " + req);
                st.executeUpdate(req);
            } else {
                JOptionPane.showMessageDialog(null, "Produit deja existant dans votre liste des favoris!");
            }
        } catch (SQLException e) {
            System.out.println("erdr      " + e.getMessage());
        }
    }

    public List<Favoris_client> Afficher_favoris() {
        List<Favoris_client> lf = new ArrayList<>();

        try {

            Statement st = c.createStatement();
            String q = " select * from favoris where iduser = 1 ";

            // q = "select * from favoris where iduser=" + 1;
            System.out.println(q);
            ResultSet rs = st.executeQuery(q);
            System.out.println(rs);

            while (rs.next() == true) {
                Favoris_client a = new Favoris_client();
                a.setId_fav(rs.getInt("id_favoris"));
                a.setId_art(rs.getInt("idprod"));
                a.setId_user(rs.getInt("iduser"));
                a.setDesc(rs.getString("descfavoris"));
                a.setReal(rs.getString("realfavoris"));

                System.out.println(a.toString());
                //a.setImg(rs.g);
                // Image image1 = new Image("file:/wamp64/www/web/uploads/images/categorieProduit/"+rs.getString(4), 120, 120, false, false);
                //a.setImg(new ImageView(image1));

                lf.add(a);

            }

        } catch (SQLException ex) {
            System.out.println("error" + ex.getMessage());
            //System.out.println("iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");

        }
        System.out.println(lf + "afficher favoris");
        return lf;
    }
    

    /*  public void Modifier_favoris(Favoris_client f, int idf, int idp) {
    try {
    Statement st2 = c.createStatement();
    String req2 = "select role from user where id_user= "
    + get_id_user();
    ResultSet rs2 = st2.executeQuery(req2);
    String role_ag = "";
    while (rs2.next()) {
    role_ag = rs2.getString(1);
    }
    Statement st1 = c.createStatement();
    String req1 = "select nom_produit from Produit where"
    // + " id_produit= " + idp/*f.getIdProd() + "";
    ResultSet rs1 = st1.executeQuery(req1);
    String nom_prod = "";
    while (rs1.next()) {
        nom_prod = rs1.getString(1);
    }
    
    if (role_ag.equals("client")) {
        PreparedStatement ps = c.prepareStatement("update favoris set "
                + "id_produit=?  ,"
                + "nom_produit=?,disponibilite=? where id_favoris=?");
        
        ps.setInt(1, idp);
        ps.setString(3, nom_prod);
        ps.setString(4, f.getDispo());
        ps.setInt(5, idf);
        System.out.println("--------- " + ps);
        ps.executeUpdate();
    } else {
    JOptionPane.showMessageDialog(null, "Erreur!");
}
    
} catch (SQLException ex) {
System.out.println("err " + ex.getMessage());
}
}*/
    public void Supprimer_favoris(int id) {
        try {
            PreparedStatement ps = c.prepareStatement("delete from favoris where id_favoris=?");

            ps.setInt(1, id);
            System.out.println("******** " + ps);
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("err " + ex.getMessage());
        }
    }
    /* public List<Favoris_client> Afficher_favoris() {
    List<Favoris_client> lf = new ArrayList<>();
    Favoris_client f;
    try {
    
    Statement st = c.createStatement();
    String q;
    
    q = "select * from Favoris where iduser=" + 1;
    
    ResultSet rs = st.executeQuery(q);
    
    while (rs.next()) {
    
    f = new Favoris_client(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getString(5));
    lf.add(f);
    }
    } catch (SQLException ex) {
    System.out.println("er'r" + ex.getMessage());
    }
    return lf;
    }*/
        public String imagefav(int id) {
       
        String ss ="";
        try {   
        Statement st1=c.createStatement();
        String req1="select * from article where id= "+id;
        ResultSet rs1=st1.executeQuery(req1);

        while(rs1.next()){
             
             ss=rs1.getString(5);
        }         
           } catch (SQLException e) {
         System.out.println("errrr "+ e.getMessage());
        } 
        return ss;
        
        
      }

}
